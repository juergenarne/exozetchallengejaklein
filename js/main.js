$(function() {
    var the_form = $('#form_comment');
    var mail_regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    $(the_form).on('submit', function() {
        go = true;
        err = [];
        $(the_form).find('.error-summary').empty().hide();
        if ($('#input-name').val() == '') {
            err.push('<p>Der Name darf nicht leer sein!</p>');
            go = false;
        }
        if ($('#input-email').val() == '') {
            err.push('<p>Die E-Mail-Adresse darf nicht leer sein!</p>');
            go = false;
        } else if (!mail_regex.test($('#input-email').val())) {
            err.push('<p>Die E-Mail-Adresse ist nicht korrekt!</p>');
            go = false;
        }
        if ($('#input-message').val() == '') {
            err.push('<p>Die Nachricht darf nicht leer sein!</p>');
            go = false;
        } else if ($('#input-message').val().length > 500) {
            err.push('<p>Die Nachricht darf nur 500 Zeichen lang sein!</p>');
            go = false;
        }

        console.log(err.length);

        if (err.length) {
            $(the_form).find('.error-summary').append(err.join('')).show();
        }

        return go;
    });
});